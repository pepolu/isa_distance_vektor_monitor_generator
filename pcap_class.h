/**
 * Wrapper for pcap libary
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <pcap.h>
#include <iostream>

using namespace std;

class PCAP{

private:
    pcap_t *pcap_d;
    const unsigned char* frame;
    char pcap_err_buff[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr hdr;

public:

    /**
     * Opens pcap live at given interface
     * @param interface name of the interface
     * */
    PCAP(string interface);

    /**
     * Returns 1 if pcap if open, 0 if not
     * */
    int isOpen();

    /**
     * Returns captured frame
     * */
    const unsigned char* getFrame();

    /**
     * Returns captured length of the frame
     * */
    unsigned int frameCapLen();

    /**
     * Returns length of the frame
     * */
    unsigned int frameLen();

    /**
     * Closes pcap live
     * */
    void close();

    /**
     * Sends frame with given size
     * @param frame frame
     * @param size size
     * */
    int send(unsigned char* frame, int size);

    /**
     * Simple destructor
     * */
    ~PCAP();

};