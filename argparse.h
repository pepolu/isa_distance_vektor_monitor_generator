/**
 * Simple parser arguments
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <string>
#include <vector>
#include <iostream>

using namespace std;


class Arg{
public:
    Arg();
    string name = "";
    string value = "";
    int mustHaveValue = 0;
    int isSet = 0;
    int isReq = 0;
    int dup = 0;
    ~Arg();
};


class ArgParse{

private:
    vector<Arg>* arg = new vector<Arg>();
    vector<string>* unexpected = new vector<string>();
    char **argv;
    int argc;

public:
    /**
     * Set up internal values
     * */
    ArgParse(int argc, char **argv);

    /**
     * Add option
     * @param opt   option name
     * @param req   is required
     * */
    void addOpt(string opt, int req);

    /**
     * Add argument
     * @param tag   argument name
     * @param req   is required
     * */
    void addArg(string tag, int req);

    /**
     * Parse set arguments
     * */
    void parse();

    /**
     * Returns 1 if option is set, else zero
     * */
    int getOpt(string opt);

    /**
     * Returns argument value
     * */
    string getArg(string tag);

    /**
     * Debug function
     * */
    void argDump();

    /**
     * Simple destructor
     * */
    ~ArgParse();
};