/**
 * RIP response program
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */


#include <iostream>
#include <string>
#include <vector>
#include "pcap_class.h"
#include <pcap.h>
#include "argparse.h"
#include "rip_generator.h"
#include <ifaddrs.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>

#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>

#include <netinet/in.h>

#include "ipv6_parser.h"

using namespace std;



/**
 * Get mac address, ipv6 address and ipv6 link-local address from the interface
 * */
int getMACandIPv6(string interface, u_int8_t mac[6], u_int8_t ipv6_link_local[16], u_int8_t ipv6[16]);


/**
 * Simple function to print mac addess
 * */
void printMAC(u_int8_t mac[6]){
    for(int i = 0; i < 6; i++){
        if( mac[i] < 16 )
            cout << "0";
        cout << hex << (int)mac[i];
        if( i == 5)
            break;
        cout << ":";
    }
    cout << dec;
}

int main(int argc, char *argv[]){

    ArgParse arg(argc, &*argv);
    arg.addArg("-i", 1);    // interface
    arg.addArg("-r", 1);    // route
    arg.addArg("-n", 0);    // next-hop || implicit ::
    arg.addArg("-m", 0);    // metric    || implicit 1
    arg.addArg("-t", 0);    // route tag  || implicit 0
    arg.addArg("-p", 0);    // password (probably ment as plain text)
    arg.parse();
    string interface = arg.getArg("-i");


    // get -r (route), parse it and save in ipv6_route
    string route = arg.getArg("-r");
    u_int8_t ipv6_route[16];
    int ipv6_route_mask = IPv6Parser().parseIPv6WithMask(route, ipv6_route);
    if(ipv6_route_mask == 0){
        cerr << "wrong ipv6 format" << endl;
        return 1;
    }


    // get interface MAC address, link-local ipv6, unique global ipv6 is not important
    u_int8_t mac[6];
    u_int8_t ipv6_link_local[16];
    int got_if_addresser = getMACandIPv6(interface, mac, ipv6_link_local, NULL);

    if( (got_if_addresser & 3) == 3 ){
        cerr << "Couldn't obtain MAC and Link-local IPv6 address" << endl;
        return 1;
    }
    if( (got_if_addresser & 1) == 1 ){
        cerr << "Couldn't obtain MAC address" << endl;
        return 1;
    }
    if( (got_if_addresser & 2) == 2 ){
        cerr << "Couldn't obtain Link-local IPv6 address" << endl;
        return 1;
    }

    // Print Information about sending interface
    cout << "Sending from " <<  interface << ", MAC(";
    printMAC(mac);
    cout << "), Link-local-IPv6(";
    IPv6Parser().printIPv6(ipv6_link_local);
    cout << ")" << endl;

    // craft RIPng message and also load optional arguments
    string n_hop = arg.getArg("-n");
    u_int8_t next_hop[16];
    if (n_hop == "")
        memset(next_hop, 0, 16);
    else
        IPv6Parser::parseIPv6(n_hop, next_hop);

    string tArg = arg.getArg("-t");
    int tag = 0;
    if( tArg != "" )
        tag = stoi(tArg);

    string metricArg = arg.getArg("-m");
    int metric = 1;
    if( metricArg != "" )
        metric = stoi(metricArg);

    // craft message and save it in frame
    unsigned char frame[1024];
    int response_len = RIPGenerator().makeRIPngResWithNH(mac, ipv6_link_local, ipv6_route, ipv6_route_mask, next_hop, tag, metric, frame);

    // open pcap_live
    PCAP* pcap = new PCAP( interface );
    if( pcap->isOpen() == 0 ){
        cerr << "PCAP sniffer is not open" << endl;
        return 1;
    }

    // send frame and print length
    cerr << "Sent: " << pcap->send(frame, response_len) << " bytes" << endl;

    return 0;
}

int getMACandIPv6(string interface, u_int8_t mac[6], u_int8_t ipv6_link_local[16], u_int8_t ipv6[16]){
    struct ifaddrs *ifaddr;
    char host[NI_MAXHOST];
    int ret = 7;
    memset(host, 0, NI_MAXHOST);
    if (getifaddrs(&ifaddr) == -1)
        return 0;
    for(; ifaddr != NULL; ifaddr = ifaddr->ifa_next){
        if (ifaddr->ifa_addr == NULL)
            continue;

        if( string(ifaddr->ifa_name) == interface && ifaddr->ifa_addr->sa_family == AF_PACKET ){
            struct sockaddr_ll* s = (struct sockaddr_ll*)ifaddr->ifa_addr;
            if( mac != NULL && s->sll_addr != NULL){
                memcpy(mac, s->sll_addr, 6);
                ret--;
            }
        }

        if( string(ifaddr->ifa_name) == interface && ifaddr->ifa_addr->sa_family == AF_INET6){
            struct sockaddr_in6* s  = (struct sockaddr_in6*)ifaddr->ifa_addr;
            if( s->sin6_addr.s6_addr != NULL ){
                if( s->sin6_addr.s6_addr[0] == 0xFE && (s->sin6_addr.s6_addr[1] & 192) == 0x80){
                    if( ipv6_link_local != NULL ){
                        memcpy(ipv6_link_local, s->sin6_addr.s6_addr, 16);
                        ret -= 2;
                    }
                }
                else{
                    if( ipv6 != NULL ){
                        memcpy(ipv6, s->sin6_addr.s6_addr, 16);
                        ret -= 4;
                    }
                }
            }
        }
    }

    return ret;
}
