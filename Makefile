CC=gcc
FLAGS=-std=gnu++11
CPP_FLAG=-lstdc++
PCAP_FLAG=-lpcap


all: myripsniffer myripresponse

clear:
	rm *.o
	rm myripsniffer
	rm myripresponse

rip_analyser.o: rip_analyser.cpp
rip_analyser.cpp: rip_analyser.h

argparse.o: argparse.cpp
argparse.cpp: argparse.h

pcap_class.o: pcap_class.cpp
pcap_class.cpp: pcap_class.h

rip_generator.o: rip_generator.cpp
rip_generator.cpp: rip_generator.h

ipv6_parser.o: ipv6_parser.cpp
ipv6_parser.cpp: ipv6_parser.h

myripsniffer.o: myripsniffer.cpp

myripresponse.o: myripresponse.cpp

myripsniffer: myripsniffer.o pcap_class.o argparse.o rip_analyser.o ipv6_parser.o
	$(CC) $(FLAGS) myripsniffer.o pcap_class.o argparse.o rip_analyser.o ipv6_parser.o -o myripsniffer $(CPP_FLAG) $(PCAP_FLAG)

myripresponse:  myripresponse.o pcap_class.o argparse.o rip_generator.o ipv6_parser.o
	$(CC) $(FLAGS) myripresponse.o pcap_class.o argparse.o rip_generator.o ipv6_parser.o -o myripresponse $(CPP_FLAG) $(PCAP_FLAG)

myripresponse.o:
	$(CC) $(FLAGS) -c myripresponse.cpp

rip_generator.o:
	$(CC) $(FLAGS) -c rip_generator.cpp

myripsniffer.o:
	$(CC) $(FLAGS) -c myripsniffer.cpp

pcap_class.o:
	$(CC) $(FLAGS) -c pcap_class.cpp

argparse.o:
	$(CC) $(FLAGS) -c argparse.cpp

rip_analyser.o:
	$(CC) $(FLAGS) -c rip_analyser.cpp

ipv6_parser.o:
	$(CC) $(FLAGS) -c ipv6_parser.cpp
