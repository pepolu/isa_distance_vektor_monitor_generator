#include "pcap_class.h"
#include <iostream>


using namespace std;

PCAP::PCAP(string interface){
    if( (this->pcap_d = pcap_open_live(interface.c_str(), BUFSIZ, 1, 10, this->pcap_err_buff)) == NULL)
        cerr <<  this->pcap_err_buff << endl;
}

int PCAP::send(unsigned char* frame, int size){
    return pcap_inject(pcap_d, frame, size);
}

int PCAP::isOpen(){
    if (this->pcap_d == NULL)
        return 0;
    else
        return 1;
}

const unsigned char* PCAP::getFrame(){
    return pcap_next(this->pcap_d, &this->hdr);
}

unsigned int PCAP::frameCapLen(){
    return this->hdr.caplen;
}

unsigned int PCAP::frameLen(){
    return this->hdr.len;
}

PCAP::~PCAP(){
    PCAP::close();
}

void PCAP::close(){
    pcap_close(this->pcap_d);
}