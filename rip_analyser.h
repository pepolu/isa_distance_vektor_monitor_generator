/**
 * RIP analyser analyses captured frames
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <string>
#include <net/ethernet.h>   // provides struct struct ether_header
#include <netinet/ip.h>     // provides struct ip_header
#include <netinet/ip6.h>     // provides struct ip_header
#include <netinet/udp.h>    // provides udp_header
#include <sys/types.h>       // provides those wacky data types in the headers
#include "rip_header.h"
#include <vector>
#include <arpa/inet.h>
#include <iostream>

#define RIP_PORT 520
#define RIPNG_PORT 521

using namespace std;

class RIPAnalyser{

private:
    rip_header_t rip_header;
    rip_union_entry_t rip_route_entry[55];

    struct ether_header ether_field;
    struct ip ip_packet;
    struct ip6_hdr ipv6_packet;
    struct udphdr udp_segment;

    int is_ipv4 = 0;
    int entry_count = 0;


public:
    /**
     * Easy constructor
     * */
    RIPAnalyser();


    /**
     * Parse RIP message
     * Gradualy points structures to the frame and extracts data to determine wheather rip message was
     * captured. All data are copied into private structure variables that are available using get methods bellow
     * @param frame captured frame
     * @param len   frame length
     * @return      0 if not rip message, 1 if valid rip message
     * */
    int analyseRIPMessage(const unsigned char* frame, int len);

    /**
     * Returns captured RIP header
     * */
    rip_header_t getRIPHeader();

    /**
     * Returns ether header
     * */
    struct ether_header getEtherHeader();

    /**
     * Returns ip header
     * */
    struct ip       getIPHeader();

    /**
     * Returns ipv6 header
     * */
    struct ip6_hdr  getIPv6Header();

    /**
     * Returns IP version of captured rip message
     * */
    int isIPv4();

    /**
     * Returns count of RIP route entries
     * */
    int ripEntryCount();

    /**
     * Returns RIP route entry at given exit
     * @param index index of route entry
     * */
    rip_route_entry_t   getRIPEntry(int index);

    /**
     * Returns RIPng route entry at given exit
     * @param index index of route entry
     * */
    ripng_route_entry_t getRIPngEntry(int index);

    /**
     * Returns rip plaint text authentication
     * @param index index of plaint text authentication
     * */
    rip_plain_text_t    getRIPplainTextAuth(int index);

    /**
     * Returns rip message digest authentication
     * @param index index of the message digest field
     * */
    rip_md5_t getRIPmd5(int index);

    /**
     * Returns message digest data trailer
     * @param index index of the data trailer
     * */
    rip_data_trailer_t getRIPDataTrailer(int index);

    ~RIPAnalyser();
};