/**
 * This header provides structures that allow accessing
 * data in capured frames
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <sys/types.h>       // provides those wacky data types in the headers


#define RIP_RTE_SIZE 20
#define RIP_HEADER_SIZE 4


typedef struct{
    u_int8_t command;
    u_int8_t version;
    u_int16_t unset;
}rip_header_t;


typedef struct{
    u_int16_t family;
    u_int16_t unset;
    u_int32_t ip;
    u_int32_t mask;
    u_int32_t next_hop;
    u_int32_t metric;
}rip_route_entry_t;


typedef struct{
    union{
        u_int8_t bit8[16];
        u_int16_t bit16[8];
        u_int32_t bit32[4];
        u_int64_t bit64[2];
    }prefix;
    u_int16_t tag;
    u_int8_t p_len;
    u_int8_t metric;
}ripng_route_entry_t;


typedef struct{
    u_int16_t   is_auth;
    u_int16_t   auth_type;
    u_int8_t    password[16];
}rip_plain_text_t;


typedef struct{
    u_int16_t   is_auth;
    u_int16_t   auth_type;
    u_int16_t   digest_offset;
    u_int8_t    key_id;
    u_int8_t    auth_data_len;
    u_int32_t   seq_num;
    u_int64_t   zero;
}rip_md5_t;


typedef struct{
    u_int16_t   ffff;
    u_int8_t    zero;
    u_int8_t    one;
    u_int8_t    hash[16];
}rip_data_trailer_t;


typedef union{
    rip_route_entry_t   rip;
    ripng_route_entry_t ripng;
    rip_plain_text_t    plain_text;
    rip_md5_t           md5;
    rip_data_trailer_t  data_trailer;
}rip_union_entry_t;
