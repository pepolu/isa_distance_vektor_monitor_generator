#include "rip_generator.h"



#define RIPngPORT 521

u_int8_t ipv6_multi_mac[] = {0x33, 0x33, 0x00, 0x00, 0x00, 0x09};


int RIPGenerator::makeRIPngResWithNH(u_int8_t src_mac[6], u_int8_t src_ip[16],
                                u_int8_t route[16], u_int8_t mask,
                                u_int8_t next_hop[16],
                                u_int16_t route_tag, u_int8_t metric,
                                unsigned char* frame){

    int len = 0;
    // start pointing structures to the frame array
    struct ether_header* ether = (struct ether_header *)frame;

    struct ip6_hdr *ip_header;
    ip_header = (struct ip6_hdr *)(frame + sizeof(struct ether_header));

    struct udphdr *udp_header;
    udp_header = (struct udphdr *)((char *)ip_header + sizeof(struct ip6_hdr));

    rip_header_t* rip_header = (rip_header_t*)(((char *)udp_header) + sizeof(struct udphdr));
    ripng_route_entry_t* next_hop_rte = (ripng_route_entry_t*)(((char *)rip_header) + RIP_HEADER_SIZE);
    ripng_route_entry_t* rte = (ripng_route_entry_t*)(((char *)next_hop_rte) + RIP_RTE_SIZE);
    int rip_size = 4 + 20 + 20;

    // set rip params
    rip_header->command = 2;
    rip_header->unset = 0;
    rip_header->version = 1;

    // copy data into the frame
    memcpy( next_hop_rte->prefix.bit8, next_hop, 16);
    next_hop_rte->tag = 0;
    next_hop_rte->p_len = 0;
    next_hop_rte->metric = 0xFF;

    memcpy( rte->prefix.bit8, route, 16);
    rte->tag = htons(route_tag);
    rte->p_len = mask;
    rte->metric = metric;

    // set UDP params
    if (rip_size & 1)
        rip_size += 1;
    int udp_size = sizeof(struct udphdr);
    udp_header->uh_dport = htons(RIPngPORT);
    udp_header->uh_sport = htons(RIPngPORT);
    udp_header->uh_ulen = htons(rip_size + udp_size);
    udp_header->uh_sum = 0;


    // set ipv6 header
    int ip_size = sizeof(struct ip6_hdr);
    ip_header->ip6_ctlun.ip6_un1.ip6_un1_flow = htonl(0x60000000);
    ip_header->ip6_ctlun.ip6_un1.ip6_un1_hlim = 0xFF;
    ip_header->ip6_ctlun.ip6_un1.ip6_un1_nxt = IPPROTO_UDP;
    ip_header->ip6_ctlun.ip6_un1.ip6_un1_plen = htons(rip_size + udp_size);
    // set ipv6 destination ff02::9
    ip_header->ip6_dst.__in6_u.__u6_addr8[0] = 0xFF;
    ip_header->ip6_dst.__in6_u.__u6_addr8[1] = 0x02;
    memset( &ip_header->ip6_dst.__in6_u.__u6_addr8[2], 0, 13);
    ip_header->ip6_dst.__in6_u.__u6_addr8[15] = 0x09;
    // set ipv6 source
    memcpy( ip_header->ip6_src.__in6_u.__u6_addr8, src_ip, 16);


    // set ether layer
    memcpy(ether->ether_shost, src_mac, 6);
    memcpy(ether->ether_dhost, ipv6_multi_mac,  6);
    ether->ether_type = htons(ETHERTYPE_IPV6);

    // call checksum for the UDP segment
    udp_header->uh_sum = htons(RIPGenerator::UDP_IPv6_checksum(ip_header, udp_header, rip_size));

    return sizeof(ether_header) + ip_size + udp_size + rip_size;
}


u_int16_t RIPGenerator::UDP_IPv6_checksum(struct ip6_hdr* ip_header, struct udphdr* udp_header, int payload_size){
    u_int32_t sum_ipv6 = 0;
    u_int32_t sum_udp = 0;
    u_int32_t sum = 0;
    u_int16_t ret = 0;
    u_int16_t* data16 = (u_int16_t*)udp_header;

    // claculate data from ipv6 pseudo header
    data16 = (u_int16_t*)ip_header->ip6_src.__in6_u.__u6_addr16;
    for( int i = 0; i < 16; i++ ){
        sum_ipv6 += ntohs(data16[i]);
    }
    sum_ipv6 += (u_int16_t)ip_header->ip6_ctlun.ip6_un1.ip6_un1_nxt;
    sum_ipv6 += ntohs(ip_header->ip6_ctlun.ip6_un1.ip6_un1_plen);

    if( (payload_size & 1) == 1 )
        payload_size++;

    // calculate data from udp segment
    payload_size /= 2;
    data16 = (u_int16_t*)udp_header;
    for( int i = 0; i < 4 + payload_size; i++ ){
        sum_udp += ntohs(data16[i]);
    }


    sum = sum_ipv6 + sum_udp;
    while( (sum >> 16) != 0 )
        sum = (sum >> 16) + (sum & 0xffff);
    
    ret = sum^0xFFFFF;

    return ret;
}
