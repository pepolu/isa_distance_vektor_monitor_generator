#include "rip_analyser.h"


RIPAnalyser::RIPAnalyser(){

}

int RIPAnalyser::analyseRIPMessage(const unsigned char* frame, int len){
    if( frame == NULL )
        return 0;

    int pos = 0;

    // examine ethernet frame
    if( pos + sizeof(struct ether_header) > len )
        return 0;
    pos += sizeof(struct ether_header);
    struct ether_header * ether_field = (struct ether_header *)frame;
    this->ether_field = *ether_field;

    // examine internet protocol header, IPv4 or IPv6, save version for route entry examination
    void* ip_header;
    int ipv4_over_ipv6 = 1;
    struct ip * ip_packet;
    struct ip6_hdr * ipv6_packet;
    if( htons(ether_field->ether_type) == ETHERTYPE_IP ){
        if( pos + sizeof(struct ip) > len )
            return 0;
        pos += sizeof(struct ip);
        ip_packet = (struct ip *)(frame + sizeof(struct ether_header));
        if( ip_packet->ip_p != IPPROTO_UDP )
            return 0;
        ip_header = ip_packet;
        this->ip_packet = *ip_packet;
    }
    else if( htons(ether_field->ether_type) == ETHERTYPE_IPV6 ){
        if( pos + sizeof(struct ip6_hdr) > len )
            return 0;
        pos += sizeof(struct ip6_hdr);
        ipv6_packet = (struct ip6_hdr *)(frame + sizeof(struct ether_header));
        if( ipv6_packet->ip6_ctlun.ip6_un1.ip6_un1_nxt != IPPROTO_UDP )
            return 0;
        ip_header = ipv6_packet;
        ipv4_over_ipv6 = 0;
        this->ipv6_packet = *ipv6_packet;
    }
    else
        return 0;
    is_ipv4 = ipv4_over_ipv6;


    // examine UDP segment, check if src and dst port match RIP
    if( pos + sizeof(struct udphdr) > len )
        return 0;
    pos += sizeof(struct udphdr);
    struct udphdr * udp_segment;
    if( ipv4_over_ipv6 == 1 )
        udp_segment = (struct udphdr *)((char *)ip_header + sizeof(struct ip));
    else
        udp_segment = (struct udphdr *)((char *)ip_header + sizeof(struct ip6_hdr));
    if( (ntohs(udp_segment->uh_sport) != RIP_PORT && ntohs(udp_segment->uh_dport) != RIP_PORT) &&
        (ntohs(udp_segment->uh_sport) != RIPNG_PORT && ntohs(udp_segment->uh_dport) != RIPNG_PORT) )
        return 0;
    this->udp_segment = *udp_segment;


    // examine RIP header, check RIP version
    if( pos + RIP_HEADER_SIZE > len )
        return 0;
    pos += RIP_HEADER_SIZE;
    rip_header_t* rip_header = (rip_header_t*)((char *)udp_segment + sizeof(struct udphdr));
    this->rip_header = *rip_header;

    void* peak;
    peak = (void*)((char *)rip_header + RIP_HEADER_SIZE);

    // load network entries
    entry_count = 0;
    rip_union_entry_t* route_entry;
    while( (len-pos) >= RIP_RTE_SIZE ){
        pos += RIP_RTE_SIZE;
        route_entry = (rip_union_entry_t*)((char *)peak);
        this->rip_route_entry[entry_count] = *route_entry;
        if( pos < len)
            peak = (void*)((char *)peak + RIP_RTE_SIZE);
        entry_count++;
        if(entry_count == 55)
            break;
    }
    return 1;
}

int RIPAnalyser::isIPv4(){
    return is_ipv4;
}

struct ether_header RIPAnalyser::getEtherHeader(){
    return ether_field;
}

struct ip RIPAnalyser::getIPHeader(){
    return ip_packet;
}

struct ip6_hdr RIPAnalyser::getIPv6Header(){
    return ipv6_packet;
}

rip_header_t RIPAnalyser::getRIPHeader(){
    return rip_header;
}

int RIPAnalyser::ripEntryCount(){
    return entry_count;
}

rip_route_entry_t RIPAnalyser::getRIPEntry(int index){
    if( index < 0 )
        return rip_route_entry[0].rip;
    if( index > 24 )
        return rip_route_entry[24].rip;
    return rip_route_entry[index].rip;
}

ripng_route_entry_t RIPAnalyser::getRIPngEntry(int index){
    if( index < 0 )
        return rip_route_entry[0].ripng;
    if( index > 24 )
        return rip_route_entry[24].ripng;
    return rip_route_entry[index].ripng;
}

rip_plain_text_t RIPAnalyser::getRIPplainTextAuth(int index){
    if( index < 0 )
        return rip_route_entry[0].plain_text;
    if( index > 24 )
        return rip_route_entry[24].plain_text;
    return rip_route_entry[index].plain_text;
}

rip_md5_t RIPAnalyser::getRIPmd5(int index){
    if( index < 0 )
        return rip_route_entry[0].md5;
    if( index > 24 )
        return rip_route_entry[24].md5;
    return rip_route_entry[index].md5;
}

rip_data_trailer_t RIPAnalyser::getRIPDataTrailer(int index){
    if( index < 0 )
        return rip_route_entry[0].data_trailer;
    if( index > 24 )
        return rip_route_entry[24].data_trailer;
    return rip_route_entry[index].data_trailer;
}

RIPAnalyser::~RIPAnalyser(){

}