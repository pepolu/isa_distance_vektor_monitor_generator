AUTOR: Peter Lukac
LOGIN: xlukac11


!!! myripsniffer vyzaduje sudo privilegia

./myripsniffer -i <rozhraní>, kde význam parametru je následující: 
* -i: <rozhraní> udává rozhraní, na kterém má být odchyt paketů prováděn.


!!! myripresponse vyzaduje sudo privilegia

./myripresponse -i <rozhraní> -r <IPv6>/[16-128] {-n <IPv6>} {-m [0-16]} {-t [0-65535]}, kde význam parametrů je následující: 
* -i: <rozhraní> udává rozhraní, ze kterého má být útočný paket odeslán;
* -r: v <IPv6> je IP adresa podvrhávané sítě a za lomítkem číselná délka masky sítě;
* -m: následující číslo udává RIP Metriku, tedy počet hopů, implicitně 1;
* -n: <IPv6> za tímto parametrem je adresa next-hopu pro podvrhávanou routu, implicitně ::;
* -t: číslo udává hodnotu Router Tagu, implicitně 0.
