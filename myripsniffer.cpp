/**
 * RIP sniffer program
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <iostream>
#include <string>
#include <vector>
#include "pcap_class.h"
#include <pcap.h>
#include "argparse.h"
#include "rip_analyser.h"
#include "ipv6_parser.h"
#include <sys/types.h>



using namespace std;


/**
 * Just a simple function to print mac address in pretty format
 * */
void printMAC(u_int8_t mac[6]){
    for(int i = 0; i < 6; i++){
        if( mac[i] < 16 )
            cout << "0";
        cout << hex << (int)mac[i];
        if( i == 5)
            break;
        cout << ":";
    }
    cout << dec;
}

/**
 * Print IPv4 address
 * */
void printIPv4(u_int32_t ip){
    cout << (ip >> 24) << "." << ((ip << 8) >> 24) << "." << ((ip << 16) >> 24) << "." << ((ip << 24) >> 24);
}


int main(int argc, char *argv[]){


    // set up argument parsing
    ArgParse arg(argc, &*argv);
    arg.addArg("-i", 1);
    arg.parse();
    string interface = arg.getArg("-i");

    // create pcap to sniff the iterface
    PCAP* pcap_sniffer = new PCAP( interface );

    if( pcap_sniffer->isOpen() == 0 ){
        cerr << "PCAP sniffer is not open" << endl;
        exit(1);
    }

    RIPAnalyser* analyser = new RIPAnalyser();
    unsigned long counter = 0;
    // main loop in whitch we capture frames and process them
    while(1){
        const unsigned char* frame;
        // get non NULL frame
        while( (frame = pcap_sniffer->getFrame()) == NULL ){}
        int len = pcap_sniffer->frameCapLen();

        // if rip message was captured and parsed, start printing
        if( analyser->analyseRIPMessage(frame, len) == 1){
            cout << "No. " << ++counter << endl;

            // recived IPv4 message
            if( analyser->isIPv4() == 1 ){
                // print source mac address and ipv4 address
                cout << "SRC: MAC: ";
                printMAC(analyser->getEtherHeader().ether_shost);
                cout << "\tSRC IP: ";
                printIPv4( ntohl(analyser->getIPHeader().ip_src.s_addr) );
                cout << endl;

                // print basic RIP header infomation
                int has_data_trailer = 0;
                cout << "RIPv" << (int)analyser->getRIPHeader().version << ":" << endl;
                cout << "\tcommand: " << (int)analyser->getRIPHeader().command << endl << endl;
                // loop used to process rip route entries and authentication fields
                for(int i = 0, re = 1; i < analyser->ripEntryCount(); i++){
                    if( i == analyser->ripEntryCount() - 1 && has_data_trailer == 1 )
                        break;

                    if( analyser->getRIPplainTextAuth(i).is_auth == 0xffff && ntohs(analyser->getRIPplainTextAuth(i).auth_type) == 2 ){
                        cout << "\tAuthentication: Simple Password (2)" << endl;
                        unsigned char password[17]; password[16] = 0;
                        memcpy(password, analyser->getRIPplainTextAuth(i).password, 16);
                        cout << "\t\tpassword: " << password << endl << endl;
                        continue;
                    }
                    if( analyser->getRIPplainTextAuth(i).is_auth == 0xffff && ntohs(analyser->getRIPplainTextAuth(i).auth_type) == 3 ){
                        cout << "\tAuthentication: Keyed Message Digest (3)" << endl;
                        cout << "\t\tDigest offset: " << ntohs(analyser->getRIPmd5(i).digest_offset) << endl;
                        cout << "\t\tKey ID: " << analyser->getRIPmd5(i).key_id << endl;
                        cout << "\t\tAuth Data Len: " << analyser->getRIPmd5(i).auth_data_len << endl;
                        cout << "\t\tSeq num: " << ntohl(analyser->getRIPmd5(i).seq_num) << endl;
                        cout << "\t\tAuthentication Data Trailer: ";
                        for( int j = 0; j < 16; j++ ){
                            if( analyser->getRIPDataTrailer(analyser->ripEntryCount()-1).hash[j] < 16 )
                                cout << "0";
                            cout << hex << (int)analyser->getRIPDataTrailer(i).hash[j];
                        }
                        cout << dec;
                        cout << endl << endl;
                        has_data_trailer = 1;
                        continue;
                    }

                    cout << "\tRIPv" << (int)analyser->getRIPHeader().version << " route entry " << (re++) << endl;
                    if( analyser->getRIPHeader().version == 1 ){
                        cout << "\t\tAddress Family: " << (unsigned)ntohs(analyser->getRIPEntry(i).family) << endl;
                        cout << "\t\tIP Address: "; printIPv4( ntohl(analyser->getRIPEntry(i).ip) ); cout << endl;
                        cout << "\t\tmetric: " << (unsigned)ntohl(analyser->getRIPEntry(i).metric) << endl;
                    }
                    else{
                        cout << "\t\tAddress Family: " << (unsigned)ntohs(analyser->getRIPEntry(i).family) << endl;
                        cout << "\t\tRoute Tag: " << (unsigned)ntohs(analyser->getRIPEntry(i).unset) << endl;
                        cout << "\t\tIP Address: "; printIPv4( ntohl(analyser->getRIPEntry(i).ip) ); cout << endl;
                        cout << "\t\tMask: "; printIPv4( ntohl(analyser->getRIPEntry(i).mask) ); cout << endl;
                        cout << "\t\tNext-hop: "; printIPv4( ntohl(analyser->getRIPEntry(i).next_hop) ); cout << endl;
                        cout << "\t\tmetric: " << (unsigned)ntohl(analyser->getRIPEntry(i).metric) << endl;
                    }
                    cout << endl;
                }
            }
            // recived IPv6 message
            else{
                // print source mac address and ipv4 address
                cout << "SRC: MAC: ";
                printMAC(analyser->getEtherHeader().ether_shost);
                cout << "\tSRC IPv6: ";
                IPv6Parser().printIPv6( analyser->getIPv6Header().ip6_src.__in6_u.__u6_addr8 );
                cout << endl;

                // print basic RIP header infomation
                int has_data_trailer = 0;
                cout << "RIPng (version: " << (int)analyser->getRIPHeader().version << ")" << endl;
                cout << "\tcommand: " << (int)analyser->getRIPHeader().command << endl << endl;
                // loop used to process rip route entries and authentication fields
                for(int i = 0, re = 1; i < analyser->ripEntryCount(); i++){
                    if( i == analyser->ripEntryCount() - 1 && has_data_trailer == 1 )
                        break;

                    if( analyser->getRIPplainTextAuth(i).is_auth == 0xffff && ntohs(analyser->getRIPplainTextAuth(i).auth_type) == 2 ){
                        cout << "\tAuthentication: Simple Password (2)" << endl;
                        unsigned char password[17]; password[16] = 0;
                        memcpy(password, analyser->getRIPplainTextAuth(i).password, 16);
                        cout << "\t\tpassword: " << password << endl << endl;
                        continue;
                    }
                    if( analyser->getRIPplainTextAuth(i).is_auth == 0xffff && ntohs(analyser->getRIPplainTextAuth(i).auth_type) == 3 ){
                        cout << "\tAuthentication: Keyed Message Digest (3)" << endl;
                        cout << "\t\tDigest offset: " << ntohs(analyser->getRIPmd5(i).digest_offset) << endl;
                        cout << "\t\tKey ID: " << analyser->getRIPmd5(i).key_id << endl;
                        cout << "\t\tAuth Data Len: " << analyser->getRIPmd5(i).auth_data_len << endl;
                        cout << "\t\tSeq num: " << ntohl(analyser->getRIPmd5(i).seq_num) << endl;
                        cout << "\t\tAuthentication Data Trailer: ";
                        for( int j = 0; j < 16; j++ ){
                            if( analyser->getRIPDataTrailer(analyser->ripEntryCount()-1).hash[j] < 16 )
                                cout << "0";
                            cout << hex << (int)analyser->getRIPDataTrailer(i).hash[j];
                        }
                        cout << dec;
                        cout << endl << endl;
                        has_data_trailer = 1;
                        continue;
                    }

                    cout << "\tRIPng route entry " << (re++) << endl; 

                    cout << "\t\tIPv6 Prefix: ";  IPv6Parser().printIPv6( analyser->getRIPngEntry(i).prefix.bit8 ); cout << endl;
                    cout << "\t\tPrefix Length: " << (unsigned)analyser->getRIPngEntry(i).p_len << endl;
                    cout << "\t\tRoute Tag: " << (unsigned)ntohs(analyser->getRIPngEntry(i).tag) << endl;
                    cout << "\t\tMetric: " << (unsigned)analyser->getRIPngEntry(i).metric << endl << endl;

                }
            }

            cout << endl;
        }
    }

    return 0;
}
