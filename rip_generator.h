/**
 * RIP generator creates RIP messages
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <iostream>
#include <sys/types.h>
#include "rip_header.h"
#include <net/ethernet.h>   // provides struct struct ether_header
#include <netinet/ip.h>     // provides struct ip_header
#include <netinet/ip6.h>     // provides struct ip_header
#include <netinet/udp.h>    // provides udp_header
#include <cstring>


using namespace std;

class RIPGenerator{

public:

    /**
     * Function crafting RIPng message
     * Function crafts entire message from bottom ether layer to RIPng body. It also calculates checksum.
     * @para src_mac    source mac address for the ether layer
     * @param src_ip    source ipv6 address
     * @param ipv6  prefix to be exchanged
     * @param mask  prefix length
     * @param next_hop  next hop
     * @param route_tag route tag
     * @param metric    metric
     * */
    static int makeRIPngResWithNH(u_int8_t src_mac[6], u_int8_t src_ip[16],
                                    u_int8_t route[16], u_int8_t mask,
                                    u_int8_t next_hop[16],
                                    u_int16_t route_tag, u_int8_t metric,
                                    unsigned char* frame);
    
    
    /**
     * Calculates checksum for the packet
     * @param ip_header ipv6 header
     * @param udp_header    udp header
     * @param payload_size  rip message size
     * */
    static u_int16_t UDP_IPv6_checksum(struct ip6_hdr* ip_header, struct udphdr* udp_header, int payload_size);

};