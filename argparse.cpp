#include "argparse.h"


Arg::Arg(){

}

Arg::~Arg(){
    
}


ArgParse::ArgParse(int argc, char **argv){
    this->argv = &*argv;
    this->argc = argc;
}

void ArgParse::addOpt(string opt, int req){
    Arg arg_1;
    arg_1.name = opt;
    arg_1.isReq = req;
    arg_1.mustHaveValue = 0;
    this->arg->push_back(arg_1);
}

void ArgParse::addArg(string tag, int req){
    Arg arg_1;
    arg_1.name = tag;
    arg_1.isReq = req;
    arg_1.mustHaveValue = 1;
    this->arg->push_back(arg_1);
}

void ArgParse::parse(){
    for( int i = 1; i < argc; i++ ){
        string arg = argv[i];
        int wasSet = 0;
        for( int j = 0; j < this->arg->size(); j++){
            if( arg ==  this->arg->operator[](j).name ){
                if( this->arg->operator[](j).isSet == 1 ){
                    this->arg->operator[](j).dup = 1;
                    wasSet = 1;
                    break;
                }
                this->arg->operator[](j).isSet = 1;
                wasSet = 1;
                if( this->arg->operator[](j).mustHaveValue == 1 ){
                    if( i + 1 == argc ){
                        break;
                    }
                    else{
                        string value = argv[i+1];
                        if( value.substr(0,1) == "-" ){
                            break;
                        }
                        else{
                            this->arg->operator[](j).value = argv[i+1];
                            i++;
                            break;
                        }
                    }
                }
            }
        }
        if( wasSet == 0 )
            this->unexpected->push_back(arg);
    }

    int allOk = 1;
    for( int i = 0; i < this->arg->size(); i++){
        if( this->arg->operator[](i).isReq == 1 && this->arg->operator[](i).isSet == 0 ){
            cerr << "argument [" << this->arg->operator[](i).name << "] is required but not set" << endl;
            allOk = 0;
        }
        if( this->arg->operator[](i).isSet == 1 && this->arg->operator[](i).mustHaveValue == 1 and this->arg->operator[](i).value == "" ){
            cerr << "argument [" << this->arg->operator[](i).name << "] requires value but no given" << endl;
            allOk = 0;
        }
        if( this->arg->operator[](i).dup == 1 ){
            cerr << "argument [" << this->arg->operator[](i).name << "] is duplicated" << endl;
            allOk = 0;
        }
    }

    for( int i = 0; i < this->unexpected->size(); i++ ){
        cerr << "unexpected argument: " << this->unexpected->operator[](i) << endl;
    }

    if(allOk == 0)
        exit(10);
}

void ArgParse::argDump(){
    cerr << "Arg dump:" << endl;
    for( int i = 0; i < this->arg->size(); i++){
        if( this->arg->operator[](i).mustHaveValue == 1){
            cerr << "arg " << i << ": " << this->arg->operator[](i).name << endl;
            cerr << "\tvalue: " << this->arg->operator[](i).value << endl;
            cerr << "\tset: " << this->arg->operator[](i).isSet << endl << endl;
        }
        else{
            cerr << "arg " << i << ": " << this->arg->operator[](i).name << endl;
            cerr << "\tset: " << this->arg->operator[](i).isSet << endl << endl;
        }
    }
}

int ArgParse::getOpt(string opt){
    for( int i = 0; i < this->arg->size(); i++){
        if( this->arg->operator[](i).mustHaveValue == 0 && this->arg->operator[](i).name == opt )
            return 1;
    }
    return 0; 
}

string ArgParse::getArg(string tag){
    for( int i = 0; i < this->arg->size(); i++){
        if( this->arg->operator[](i).mustHaveValue == 1 && this->arg->operator[](i).name == tag )
            return this->arg->operator[](i).value;
    }
    return "";
}

ArgParse::~ArgParse(){

}