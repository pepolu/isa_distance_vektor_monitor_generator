#include "ipv6_parser.h"


int IPv6Parser::parseIPv6WithMask(string prefix_and_mask, u_int8_t ipv6[16]){
    if(ipv6 == NULL)
        return 0;

    int slash = prefix_and_mask.find("/");

    if( slash == -1 || slash > 42 ){
        cerr << "missing prefix-mask separator" << endl;
        return 0;
    }
    string mask = prefix_and_mask.substr(slash+1, prefix_and_mask.length() - slash + 1);
    string input = prefix_and_mask.substr(0, slash);

    if( IPv6Parser::parseIPv6(input, ipv6) == 0)
        return 0;

    return stoi(mask);
}

int IPv6Parser::parseIPv6(string input, u_int8_t ipv6[16]){
    if(ipv6 == NULL)
        return 0;

    memset(ipv6, 0, 16);
    return inet_pton( AF_INET6, input.c_str(), (void*)ipv6);
}

void IPv6Parser::printIPv6raw(u_int8_t ipv6[16]){
    for( int i = 0; i < 15; i+=2 ){
        if( (ipv6[i] & 240) == 0 )
            cout << "0";
        cout << hex << (unsigned)ipv6[i];
        if( (ipv6[i+1] & 240) == 0 )
            cout << "0";
        cout << hex << (unsigned)ipv6[i+1];
        if( i == 14)
            break;
        cout << ":";
    }
}

void IPv6Parser::printIPv6(u_int8_t ipv6[16]){
    if(ipv6 == NULL)
        return;

    char buff[42];
    memset(buff, 0, 42);
    inet_ntop( AF_INET6, (void *)ipv6, buff, 42);
    cout << buff;
}