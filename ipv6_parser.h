/**
 * Parser parses recived frames
 * 
 * @author Peter Lukac
 * @author xlukac11
 * */

#include <string>
#include <iostream>
#include <algorithm>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include <sstream>
#include <vector>


using namespace std;


class IPv6Parser{

public:

    /**
     * Parses prefix with prefix lenghth and sets ipv6 in pointer and returns pref. l.
     * */
    static int parseIPv6WithMask(string prefix_and_mask, u_int8_t ipv6[16]);

    /**
     * Parses just ipv6 string, returns 1 if ok, else 0
     * */
    static int parseIPv6(string input, u_int8_t ipv6[16]);

    /**
     * Print ipv6
     * */
    static void printIPv6raw(u_int8_t ipv6[16]);

    /**
     * Print ipv6
     * */
    static void printIPv6(u_int8_t ipv6[16]);
};